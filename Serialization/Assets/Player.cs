﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] PlayerData data;

    // Start is called before the first frame update
    void Start()
    {
        data = new PlayerData();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            Debug.Log("Added item " + data.inventory.Count);
            data.inventory.Add(new Item("Item " + data.inventory.Count));
            data.inventory[0].itemName = "OH!";
        }

        if(Input.GetKeyDown(KeyCode.P))
        {
            // Application.persistantDataPath is a safe place to store save files and other junk that you
            // only want on this computer.
            // On Windows, it is something like C:\Users\<LocalUser>\AppData\LocalLow\<CompanyName>\<ProjectName>
            Debug.Log("Saving to " + Application.persistentDataPath + "...");

            string json = JsonUtility.ToJson(data, true);

            StreamWriter writer = new StreamWriter(Application.persistentDataPath + "/save1.txt");

            writer.Write(json);

            writer.Close();


            Debug.Log("Saved");
        }


        if(Input.GetKeyDown(KeyCode.F5))
        {
            Debug.Log("Loading...");

            StreamReader reader = new StreamReader(Application.persistentDataPath + "/save1.txt");

            string json = reader.ReadToEnd();

            reader.Close();


            data = JsonUtility.FromJson<PlayerData>(json);


            Debug.Log("Loaded");
        }
    }
}
