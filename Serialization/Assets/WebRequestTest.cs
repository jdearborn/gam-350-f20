﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[System.Serializable]
public class CatFactsData
{
    public List<CatFact> all = new List<CatFact>();
}

[System.Serializable]
public class CatFact
{
    public string text;
    public int upvotes;
}

/*

{
"all": [
{
"_id": "591d9b2f227c1a0020d26823",
"text": "Every year, nearly four million cats are eaten in China as a delicacy.",
"type": "cat",
"user": {
"_id": "5a9ac18c7478810ea6c06381",
"name": {
"first": "Alex",
"last": "Wohlbruck"
}
},
"upvotes": 10,
"userUpvoted": null
},
...
 
 */

public class WebRequestTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(GetCatFacts());
    }

    IEnumerator GetCatFacts()
    {
        UnityWebRequest myRequest = UnityWebRequest.Get("https://cat-fact.herokuapp.com/facts");
        myRequest.SetRequestHeader("Content-Type", "application/json");

        yield return myRequest.SendWebRequest();



        if(myRequest.isNetworkError || myRequest.isHttpError)
        {
            Debug.Log("Couldn't get sweet, sweet cat facts: " + myRequest.error);

            yield break;  // Ends coroutine?
        }

        Debug.Log("Success!!");
        string json = myRequest.downloadHandler.text;

        Debug.Log(json);

        CatFactsData cfd = JsonUtility.FromJson<CatFactsData>(json);

        if(cfd == null || cfd.all == null)
        {
            Debug.Log("Failed to parse JSON");
            yield break;
        }

        for(int i = 0; i < cfd.all.Count; ++i)
        {
            Debug.Log(cfd.all[i].upvotes + ": " + cfd.all[i].text);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
