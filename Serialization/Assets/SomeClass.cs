﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SomeClass : MonoBehaviour
{
    private int myNumber = 0;

    // C# property
    public int otherNumber {
        get {
            return myNumber;
        }
        set {
            myNumber = value;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        otherNumber = 5;
        Debug.Log(transform.position.x);
        //transform.position.x = 5;
        //transform.position = new Vector3(5f, 0f, 0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public int DoThings()
    {
        myNumber++;

        return myNumber;
    }


    // "Getter" / Accessor
    public int GetMyNumber()
    {
        return myNumber;
    }

    // "Setter" / Mutator
    public void SetMyNumber(int newNumber)
    {
        myNumber = newNumber;
    }
}
