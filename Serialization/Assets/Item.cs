﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Item
{
    [SerializeField] private string itemName7;

    // C# property to generate a getter and setter for my private backing variable,
    // but the interface will stay constant even if I refactor my private variables.
    public string itemName {
                             get {
                                    return itemName7;
                                 }
                             set {
                                    itemName7 = value;
                                 }
                           }

    public Item(string newName)
    {
        itemName = newName;
    }

    /*
    // Accessor (getter)
    public string GetItemName()
    {
        return itemName1;
    }

    // Mutator (setter)
    public void SetItemName(string newName)
    {
        itemName1 = newName;
    }*/
}
